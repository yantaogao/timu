## cuda doc
1. 编译 main.cu (环境: CentOS7, RTX3090, cuda ToolKit 11.3)
2. Peer-to-Peer Memory Access (cuda code)
3. Concurrent Kernel Execution (cuda code)
4. Concurrent copy and execute (cuda code)
5. cuda memorys (列举并说明)
6. Broadcast of a single value across warp (cuda code)
7. Reduction across a warp (cuda code)
8. #pragma unroll

## cuda lib
1. 实现 cublasCdotc
```
cublasStatus_t cublasCdotc(cublasHandle_t handle, int n,
        const cuComplex       *x, int incx,
        const cuComplex       *y, int incy,
        cuComplex       *result)
```
2. 实现 cublasCaxpy
```
cublasStatus_t cublasCaxpy(cublasHandle_t handle, int n,
        const cuComplex       *alpha,
        const cuComplex       *x, int incx,
        cuComplex             *y, int incy)
```

## cuda PWmat
1. c(i)=<a(:,i)|b(:,i)> (complex*16 :: c(N), a(M,N), b(M,N))
2. a_k = forward_fft(a_r) (complex*16 :: a_r(M1,M2,M3), a_k(M1,M2,M3))
3. H的LU分解 (complex*16 :: H(M,M))
4. \sum_{ij} |beta(:,i)>D_{ij}<beta(:,j)|psi(:)> (complex*16 :: beta(M,N), D(N,N), psi(M))

## mpi
1. 数据--complex*16 :: data(M,N), 环境--共MxN个节点，将data划分为两个communicator--MPI_COMM_M 和 MPI_COMM_N, 并将data发送到不同的节点
2. 在1的基础上，计算sum(data(1:M,i))
3. 解释:
```
do iislda=1,islda
    call mpi_reduce_scatter(rho_n_tmp2(1,iislda),rho_n_5(1,iislda),r_ns,MPI_REAL8,MPI_SUM,MPI_COMM_K_0,ierr)
enddo
```

4. 解释:
```
do ig = 1, each_nblock -1
    call MPI_ALLTOALLV(wf_input(1,(ig-1)*nnodes_k+1),&
    send_counts,sdisps,MPI_COMPLEX,wf_output(1,ig), &
    recv_counts,disps,MPI_COMPLEX,MPI_COMM_K_0,ierr)
enddo
```

## 数值
1. PCCG 求 Ax=b. 
2. 用拉格朗日插值加密数据, data(n1,n2,n3) -> data(n1L,n2L,n3L), 其中n1L>n1, n2L>n2, n3L>n3.
3. 计算\int_x0^x2 f(x) dx, 数据 x0,f(x0),x1,f(x1),x2,f(x2).
4. 写出球谐函数Y_2^-1(\theta, \phi)的球坐标形式和直角坐标形式.
5. Householder变换

## 固体物理
1. 写出: 平面波的表达式
2. 写出: bloch定理
3. 写出算符: 动能、动量、位置
4. 写出：波函数的周期性边界条件
5. 已知晶格矢量，求倒格矢
6. 写出: 费米-狄拉克分布 
7. 简述: time reversal symmetry, 并简要证明
8. 简述: 结合能的计算
9. 什么是单电子近似？

## DFT
1. 什么是密度泛函理论？
2. LDA, GGA, METAGGA, HSE
3. 什么是动能截断能?
4. 代码: DOS(E_i)=sum_E \delta(E_i-E)
5. 代码: 已知能带和总电子数，求费米能级
6. 简述自洽迭代过程
7. 写出Kohn-Sham方程
8. 写出总能的组成
9. 简述 HELLMAN-FEYNMAN THEOREM, 并简要证明
10. 
